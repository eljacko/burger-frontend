import Vue from 'vue'
import App from './App.vue'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueGeolocation from 'vue-browser-geolocation';

Vue.config.productionTip = false

Vue.use(VueMaterial)
Vue.use(VueGeolocation);
Vue.use(VueAxios, axios)

window.vue = new Vue({
  render: h => h(App),
}).$mount('#app')
